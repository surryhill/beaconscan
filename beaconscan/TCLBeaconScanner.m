//
//  TCLBeaconScanner.m
//  Bleu Broadcaster
//
//  Created by Tim Perfitt on 5/15/15.
//  Copyright (c) 2015 Twocanoes Software. All rights reserved.
//

#import "TCLBeaconScanner.h"
@interface TCLBeaconScanner ()
@property (strong) CBCentralManager * manager;
@property (nonatomic, copy) void (^scanningBlock)(NSDictionary *);
@property (nonatomic, copy) void (^errorBlock)();
@property (strong,nonatomic) CBCentralManager *centralManager;
@property (nonatomic, strong) dispatch_queue_t managerQueue;


@end
@implementation TCLBeaconScanner



-(void)stopScanning{
    [self.manager stopScan];
    
}

-(void)resumeScanning{
    
    [self kickOffScanning];
}
-(void)startScanningWithBlock:(void (^)(NSDictionary *))inScanningBlock errorBlock:(void (^)(NSString *))inErrorBlock{
    
    dispatch_queue_t _sync_queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    self.managerQueue = dispatch_queue_create("com.twocanoes.centralManagerQueue", NULL);
    
    
    self.manager = [[CBCentralManager alloc] initWithDelegate:self
                                                        queue:self.managerQueue];
    
    
    
    //    if (!self.manager) self.manager=[[CBCentralManager alloc] initWithDelegate:self queue:self.managerQueue];
    self.scanningBlock=inScanningBlock;
    self.errorBlock=inErrorBlock;
    
    [self kickOffScanning];
}
-(void)centralManagerDidUpdateState:(CBCentralManager *)central{
    [self kickOffScanning];
}
-(void)kickOffScanning{
    
    if (self.manager.state==CBCentralManagerStatePoweredOff) {
        self.errorBlock(@"Bluetooth powered off");
    }
    else  if (self.manager.state==CBCentralManagerStateResetting) {
        self.errorBlock(@"Bluetooth Resetting");
    }
    else  if (self.manager.state==CBCentralManagerStateUnsupported) {
        self.errorBlock(@"Bluetooth Unsupported");
    }
    else  if (self.manager.state==CBCentralManagerStateUnauthorized) {
        self.errorBlock(@"Bluetooth Unauthorized");
    }
    else if (self.manager.state==CBCentralManagerStatePoweredOn) {
        
        //        [self.manager stopScan];
        [self.manager scanForPeripheralsWithServices:nil options:@{ CBCentralManagerScanOptionAllowDuplicatesKey : @NO}];
    }
    
}

- (void)centralManager:(CBCentralManager *)central
 didDiscoverPeripheral:(CBPeripheral *)peripheral
     advertisementData:(NSDictionary *)advertisementData
                  RSSI:(NSNumber *)RSSI{
    
    
    
    NSData *advData=[advertisementData objectForKey:@"kCBAdvDataManufacturerData"];
    NSDictionary *serviceDataDict= (NSDictionary *)[advertisementData objectForKey:CBAdvertisementDataServiceDataKey];
    
    char beaconBytes[]={0x4c,0x00,0x02,0x15};
    char physicalBytes[]={0x4c,0x00,0x02,0x75};
    
    uint16_t majorNumber;
    uint16_t minorNumber;
    int8_t powerNumber;
    if ([[advData subdataWithRange:NSMakeRange(0, 4)] isEqualToData:[NSData dataWithBytes:&beaconBytes length:4]]) {
        
        NSData *uuid=[advData subdataWithRange:NSMakeRange(4,16)];
        NSData *majorData=[advData subdataWithRange:NSMakeRange(20,2)];
        NSData *minorData=[advData subdataWithRange:NSMakeRange(22,2)];
        NSData *powerData=[advData subdataWithRange:NSMakeRange(24,1)];
        
        
        NSUUID *regionUUID=[[NSUUID alloc] initWithUUIDBytes:[uuid bytes]];
        
        majorNumber = CFSwapInt16BigToHost(*(int*)([majorData bytes]));
        minorNumber = CFSwapInt16BigToHost(*(int*)([minorData bytes]));
        [powerData getBytes:&powerNumber length:1];
        
        self.scanningBlock(@{@"Type":@"iBeacon",
                             @"RegionUUID":regionUUID.UUIDString,
                             @"MajorNumber":[NSString stringWithFormat:@"%i",majorNumber],
                             @"MinorNumber":[NSString stringWithFormat:@"%i",minorNumber],
                             @"PowerCalibration":[NSString stringWithFormat:@"%i",powerNumber],
                             @"RSSI":[NSString stringWithFormat:@"%li",[RSSI integerValue]]
                             });
    }
    else  if (serviceDataDict !=nil  ){
        
        if (!serviceDataDict) return;
        CBUUID *uuid=(CBUUID *) [[serviceDataDict allKeys] firstObject];
        NSString *uuidString;
        if ([uuid respondsToSelector:@selector(UUIDString)]) {
            uuidString=[uuid UUIDString]; // Available since iOS 7.1
        } else {
            uuidString=[[[NSUUID alloc] initWithUUIDBytes:[[uuid data] bytes]] UUIDString]; // iOS 6.0+
        }
        
        
        
        if (![[uuidString uppercaseString] isEqualTo:@"FED8"]) {
            return;
        }
        
        
        
        int invisibleFlag=0;
        int8_t txPower=0;
        
        NSData *serviceData=[serviceDataDict objectForKey:uuid];
        [serviceData getBytes:&invisibleFlag range:NSMakeRange(0, 1)];
        [serviceData getBytes:&txPower range:NSMakeRange(1, 1)];
        
        
        NSData *encodedURL=[serviceData subdataWithRange:NSMakeRange(2, [serviceData length]-2)] ;
        NSMutableString*urlString=[NSMutableString string];
        char byte;
        
        int i;
        
        [encodedURL getBytes:&byte range:NSMakeRange(0, 1)];
        switch (byte) {
            case 0x00:
                [urlString appendString:@"http://www."];
                break;
            case 0x01:
                [urlString appendString:@"http://"];
                break;
            case 0x02:
                [urlString appendString:@"http://"];
                break;
            case 0x03:
                [urlString appendString:@"https://"];
                break;
            case 0x04:
                [urlString appendString:@"urn:uuid:"];
                break;
            default:
                break;
        }
        for (i=1;i<[encodedURL length];i++) {
            
            
            [encodedURL getBytes:&byte range:NSMakeRange(i, 1)];
            
            switch (byte) {
                case 0x00:
                    [urlString appendString:@".com/"];
                    break;
                case 0x01:
                    [urlString appendString:@".org/"];
                    break;
                case 0x02:
                    [urlString appendString:@".edu/"];
                    break;
                case 0x03:
                    [urlString appendString:@".net/"];
                    break;
                case 0x04:
                    [urlString appendString:@".info/"];
                    break;
                case 0x05:
                    [urlString appendString:@".biz/"];
                    break;
                    
                case 0x06:
                    [urlString appendString:@".gov/"];
                    break;
                    
                case 0x07:
                    [urlString appendString:@".com"];
                    break;
                    
                case 0x08:
                    [urlString appendString:@".org"];
                    break;
                    
                case 0x09:
                    [urlString appendString:@".edu"];
                    break;
                    
                case 0x0A:
                    [urlString appendString:@".net"];
                    break;
                    
                case 0x0B:
                    [urlString appendString:@".info"];
                    break;
                    
                case 0x0C:
                    [urlString appendString:@".biz"];
                    break;
                    
                case 0x0D:
                    [urlString appendString:@".gov"];
                    break;
                    
                default:
                    [urlString appendString:[NSString stringWithFormat:@"%c",byte]];
            }
            
            
        }
        self.scanningBlock(@{@"Type":@"UriBeacon",@"InvisibleFlag":[NSString stringWithFormat:@"%i",invisibleFlag],@"PowerCalibration":[NSString stringWithFormat:@"%i",txPower],@"RegionUUID":[NSString stringWithString:urlString],@"RSSI":[NSString stringWithFormat:@"%li",[RSSI integerValue]]});
        
        
        
    }
}

@end
