# beaconscan #

[![asciicast](https://asciinema.org/a/30zbctyol6ec5il6ci3owo4rk.png)](https://asciinema.org/a/30zbctyol6ec5il6ci3owo4rk)

beaconscan is a command line utility for OS X that will display information about nearby beacons.  For iBeacons, it will display the UUID, Major, Minor, Power Calibration, and RSSI.  For Physical Web Beacons (or Uribeacons), it will show the URL associated with the beacon.

## Usage ##
```
#!


----------------------
beaconscan
----------------------
beaconscan is a command line utility to browse iBeacon and Physical Beacons on OS X and print out the beacon information.
Copyright 2015 Twocanoes Labs, LLC

Usage:
beaconscan -help                      this message
beaconscan -scantime <time>           seconds to scan before exiting
beaconscan -outputType <json|plain>   change output format
```

## Downloads ##
Get it [here.](https://bitbucket.org/twocanoes/beaconscan/downloads/beaconscan.zip)

## What is an iBeacon? ##
Check out our [Beacons 101](http://twocanoes.com/beacons101) page

## What Can I Do With This? ##
Since it is a command line utility, you can use it with yours scripts to do interesting things that happen when a beacon is in range.  

## I don't see any output! ##
You need to have a beacon with iBeacon technology or a physical web beacons in order to see them.  The best beacons are over at [twocanoes.com](http://store.twocanoes.com).  Head over there and grab some. 

## Fascinating! What can I do with iBeacons today?  ##
We make a bunch of apps and services that will help you get started with your new beacons:
Check out our proximity apps [here](https://twocanoes.zendesk.com/hc/en-us/articles/202691109-Proximity-Related-Apps-from-Twocanoes-Labs)

Also check out the [10 awesome things you can do today with iBeacons](http://blog.twocanoes.com/post/68861362715/10-awesome-things-you-can-do-today-with-ibeacons) blog post.